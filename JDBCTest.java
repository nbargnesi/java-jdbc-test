import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Class.forName;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.lang.System.*;
import static java.sql.DriverManager.getConnection;

public class JDBCTest {

    private final String query;
    private final Connection conn;

    public JDBCTest(String url, String driver, String query) {
        try {
            forName(driver);
            conn = getConnection(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.query = query;
    }

    public void query() {
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            print(rs);
            rs.close();
            stmt.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            conn.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String... args) {
        if (args.length != 4) {
            err.println("Usage: DRIVER URL COUNT QUERY");
            err.println("Executes SQL queries against a JDBC URL.");
            err.print("Example: ");
            err.print("com.mysql.jdbc.Driver jdbc:mysql://localhost/db 1 ");
            err.println("\"select count(*) from table\"");
            exit(1);
        }

        String driver = args[0];
        String url = args[1];
        int count = parseInt(args[2]);
        String query = args[3];

        StringBuilder bldr = new StringBuilder();
        bldr.append("Executing query using driver %s, %d times.\n");
        bldr.append("JDBC URL: %s\n");
        bldr.append("Query: %s\n\n");
        bldr.append("[ENTER] when ready");
        String fmt = bldr.toString();
        String msg = format(fmt, driver, count, url, query);
        out.println(msg);
        try {
            in.read();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        JDBCTest test = new JDBCTest(url, driver, query);
        for (int i = 1; i <= count; i++) {
            test.query();
        }
        test.close();
    }

    private static void print(ResultSet rs) {
        final String fmt = "%s: %s";
        try {
            ResultSetMetaData rsmeta = rs.getMetaData();
            int count = rsmeta.getColumnCount();
            List<String> columns = new LinkedList<>();
            for (int i = 1; i <= count; i++) {
                columns.add(rsmeta.getColumnName(i));
            }
            while (rs.next()) {
                for (int i = 1; i <= count; i++) {
                    String col = columns.get(i - 1);
                    String str = rs.getString(i);
                    out.println(format(fmt, col, str));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
